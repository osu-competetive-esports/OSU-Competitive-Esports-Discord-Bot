import logging

logger = logging.getLogger(__name__)

async def admin_check(ctx):
    from osu_competitive_esports_bot.main import DISCORD_ADMIN_ROLES

    print('admin check', DISCORD_ADMIN_ROLES)

    for guild in ctx.bot.guilds:
        member = guild.get_member(ctx.author.id)

        print(member)
        if member is not None:
            for member_role in member.roles:
                print(member_role.name)
                for role in DISCORD_ADMIN_ROLES:
                    if role.lower() == member_role.name.lower():
                        return True
    
    return False
