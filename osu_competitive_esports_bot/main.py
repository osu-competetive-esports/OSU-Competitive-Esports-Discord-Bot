#!/usr/bin/env python3
import asyncio
import json
import logging

import discord
from apiclient.discovery import build
from discord.ext import commands
from httplib2 import Http
from oauth2client import client
from oauth2client import file as oauth_file
from oauth2client import tools

STARTED = False

DISCORD_INTENTS = discord.Intents.default()
DISCORD_INTENTS.members = True

DISCORD_BOT = commands.Bot(command_prefix=('!', '.'), intents=DISCORD_INTENTS)
DISCORD_BOT.case_insensitive=True

ROLES_TO_ADD = []
ADMIN_ROLES = []
GUEST_ROLES = []
GUEST_LOGS = []

# If modifying these scopes, delete the file token.json.
GOOGLE_API_SCOPES = 'https://www.googleapis.com/auth/spreadsheets'

with open('settings.json') as json_data:
    settings = json.load(json_data)

SPREADSHEET_ID = settings['spreadsheet']['id']
SPREADSHEET_RANGE = settings['spreadsheet']['range']

AUTH0_CLIENT_ID = settings['auth0']['client_id']
AUTH0_CLIENT_SECRET = settings['auth0']['client_secret']

DISCORD_TOKEN = settings['discord']['token']
DISCORD_VERIFICATION_ROLES = settings['discord']['roles']
DISCORD_GAME_NOTIFICATION_ROLES = settings['discord']['game_notification_roles']
DISCORD_SYSTEM_NOTIFICATION_ROLES = settings['discord']['system_notification_roles']
DISCORD_IDENTITY_ROLES = settings['discord']['identity_roles']
DISCORD_ADMIN_ROLES = settings['discord']['admin_roles']
DISCORD_SUMMON_USER = settings['discord']['summon_user']
DISCORD_JOIN_ROLES = settings['discord']['join_roles']
DISCORD_GUEST_ROLES = settings['discord']['guest_roles']
DISCORD_GUEST_LOGS = settings['discord']['guest_logs']
DISCORD_DONT_DEL_COMMANDS = settings['discord']['dont_del_commands']

OAUTH_STORE = oauth_file.Storage('token.json')
OAUTH_CREDS = OAUTH_STORE.get()
if not OAUTH_CREDS or OAUTH_CREDS.invalid:
    flow = client.flow_from_clientsecrets('google_credentials.json', GOOGLE_API_SCOPES)
    OAUTH_CREDS = tools.run_flow(flow, OAUTH_STORE)
GOOGLE_SERVICE_SHEETS = build('sheets', 'v4', cache_discovery=False, http=OAUTH_CREDS.authorize(Http()))

email_memory = {}
logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)


@DISCORD_BOT.event
@asyncio.coroutine
async def on_ready():
    global ROLES_TO_ADD
    global ADMIN_ROLES
    global GUEST_ROLES
    global GUEST_LOGS
    global STARTED

    ROLES_TO_ADD = []
    ADMIN_ROLES = []
    GUEST_ROLES = []
    GUEST_LOGS = []

    print('We have logged in as {0.user}'.format(DISCORD_BOT))

    print('Connected to the following servers:')
    for guild in DISCORD_BOT.guilds:
        print(' - ' + guild.name)
        print('\tRoles:')
        for role in guild.roles:
            if role.name in DISCORD_VERIFICATION_ROLES:
                print('\t - ' + role.name + ' <--[*role to add after verification*]')
                ROLES_TO_ADD.append(role)
            elif role.name in DISCORD_ADMIN_ROLES:
                print('\t - ' + role.name + ' <--[*admin*]')
                ADMIN_ROLES.append(role)
            elif role.name in DISCORD_GUEST_ROLES:
                print('\t - ' + role.name + ' <--[*guest*]')
                GUEST_ROLES.append(role)
            else:
                print('\t - ' + role.name)

        print('\tChannels:')
        for channel in guild.channels:
            if channel.name in DISCORD_GUEST_LOGS:
                print('\t - ' + channel.name + ' <--[*guest-log*]')
                GUEST_LOGS.append(channel)
            elif channel.name in DISCORD_DONT_DEL_COMMANDS:
                print('\t - ' + channel.name + ' <--[*don\'t delete*]')
            else:
                print('\t - ' + channel.name)
    
    # Hook commands
    if not STARTED:
        from osu_competitive_esports_bot.commands.debug import debug
        from osu_competitive_esports_bot.commands.game_notify_role import game_notify_role
        from osu_competitive_esports_bot.commands.ping import ping
        from osu_competitive_esports_bot.commands.guest_role import guest_role
        #import osu_competitive_esports_bot.commands.help
        from osu_competitive_esports_bot.commands.iam import iam
        from osu_competitive_esports_bot.commands.ihave import ihave
        from osu_competitive_esports_bot.commands.iplay import iplay
        from osu_competitive_esports_bot.commands.join import join
        from osu_competitive_esports_bot.commands.verify_code import verify_code
        from osu_competitive_esports_bot.commands.verify_email import verify_email

        DISCORD_BOT.add_command(debug)
        DISCORD_BOT.add_command(game_notify_role)
        DISCORD_BOT.add_command(ping)
        DISCORD_BOT.add_command(guest_role)
        DISCORD_BOT.add_command(join)
        DISCORD_BOT.add_command(iam)
        DISCORD_BOT.add_command(iplay)
        DISCORD_BOT.add_command(ihave)
        DISCORD_BOT.add_command(verify_email)
        DISCORD_BOT.add_command(verify_code)

    STARTED = True


@DISCORD_BOT.event
@asyncio.coroutine
async def on_guild_join(guild):
    global ROLES_TO_ADD
    global ADMIN_ROLES
    global GUEST_ROLES
    global GUEST_LOGS
    
    ROLES_TO_ADD = []
    ADMIN_ROLES = []
    GUEST_ROLES = []
    GUEST_LOGS = []

    print('Joining ' + guild.name)
    print('\tRoles:')
    for role in guild.roles:
        if role.name in DISCORD_VERIFICATION_ROLES:
            print('\t - ' + role.name + ' <--[*role to add after verification*]')
            ROLES_TO_ADD.append(role)
        if role.name in DISCORD_ADMIN_ROLES:
            print('\t - ' + role.name + ' <--[*admin*]')
            ADMIN_ROLES.append(role)
        elif role.name in DISCORD_GUEST_ROLES:
            print('\t - ' + role.name + ' <--[*guest*]')
            GUEST_ROLES.append(role)
        else:
            print('\t - ' + role.name)

    print('\tChannels:')
    for channel in guild.channels:
        if channel.name in DISCORD_GUEST_LOGS:
            print('\t - ' + channel.name + ' <--[*guest-log*]')
            GUEST_LOGS.append(channel)
        elif channel.name in DISCORD_DONT_DEL_COMMANDS:
            print('\t - ' + channel.name + ' <--[*don\'t delete*]')
        else:
            print('\t - ' + channel.name)


async def delete_message(ctx):
    if not isinstance(ctx.message.channel, discord.abc.PrivateChannel) and ctx.message.channel.name not in DISCORD_DONT_DEL_COMMANDS:
        logger.info('Deleting message.')
        await ctx.message.delete()


def get_roles_to_add():
    global ROLES_TO_ADD
    return ROLES_TO_ADD


def get_admin_roles():
    global ADMIN_ROLES
    return ADMIN_ROLES


def get_guest_roles():
    global GUEST_ROLES
    return GUEST_ROLES


def get_guest_logs():
    global GUEST_LOGS
    return GUEST_LOGS

if __name__ == '__main__':
    DISCORD_BOT.run(DISCORD_TOKEN)
