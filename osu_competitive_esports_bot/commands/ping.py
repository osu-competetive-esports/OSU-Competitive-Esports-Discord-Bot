from osu_competitive_esports_bot.main import DISCORD_BOT
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command()
async def ping(ctx):
    from osu_competitive_esports_bot.main import delete_message

    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !ping command.")

    await ctx.send(
        ctx.message.author.mention + ' - Pong!'
    )
    await delete_message(ctx)
    logger.info("!ping command complete.")


@ping.error
async def ping_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CommandError):
        await ctx.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
