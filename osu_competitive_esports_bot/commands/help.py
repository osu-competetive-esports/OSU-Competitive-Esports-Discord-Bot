from osu_competitive_esports_bot.main import DISCORD_BOT
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command()
async def help(ctx, arg):
    from osu_competitive_esports_bot.utils.admin_check import is_user_admin
    from osu_competitive_esports_bot.main import delete_message

    logger.info(ctx.message.author.mention, ctx.message.content)
    logger.info("Parsing !help command.")

    await ctx.send(
        ctx.message.author.mention + ' - \n'
        '*Note:* Examples use `!` for the start of commands, but you can also use `.`\n'
        '**=====Main Help=====**\n'
        '`!help`: This message.\n'
        '`!ping`: Check if the bot is still running. (Though, if you just ran this command, it confirms that the '
        'bot is still running.)\n'
        '**=====OSU Email Verification=====**\n'
        '`!verify-email <OSU email address>`: Verify your OSU email. Replace the `<OSU email address>` with your '
        '`osu.edu` or `buckeyemail.osu.edu` address.\n'
        '`!verify-code <code>`:  Replace the `<code>` with the verification code emailed to you.\n'
        '**=====I Play=====**\n'
        '`!iplay <game>`: Replace <game> with the game or games separated by a `,`. Use `!iplay` '
        'to list the games avalible.\n'
        '**=====Admin Only Commands=====**\n' +
        '`!debug`: Prints some basic debug information.\n'
        '`!game-notify-role <add|remove> <role name with out @>`: Multiple roles can be '
        'added/removed by separating them with a comma.\n'
    )
    await delete_message(ctx)
    logger.info("!help command complete.")


@help.error
async def help_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CommandError):
        await ctx.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
